#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll a, b;
    cin >> a >> b;
    ll max = a;
    if (b > a)
        max = b;
    ll x = (6 - max) + 1, y = 6;
    fm(i, 1, 7)
    {
        if (x % i == 0 && 6 % i == 0)
        {
            x = x / i;
            y = 6 / i;
            break;
        }
    }
    cout << x << "/" << y;
}

int main()
{

    code();
    return 0;
}

/*		Ade Rizaldi		*/
