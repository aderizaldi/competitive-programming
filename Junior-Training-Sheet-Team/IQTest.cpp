#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

bool cek_square(string arr[])
{
    bool kon = false;
    fp(i, 0, 3)
    {
        fp(j, 0, 3)
        {
            if (arr[i][j] == '#' && arr[i][j + 1] == '#' && arr[i + 1][j] == '#' && arr[i + 1][j + 1] == '#')
            {
                kon = true;
                break;
            }
            if (arr[i][j] == '.' && arr[i][j + 1] == '.' && arr[i + 1][j] == '.' && arr[i + 1][j + 1] == '.')
            {
                kon = true;
                break;
            }
        }
        if (kon)
            break;
    }
    return kon;
}

string code()
{
    string arr[4], test = "";
    bool kon = false;
    fp(i, 0, 4)
    {
        string input;
        cin >> input;
        arr[i] = input;
    }
    if (cek_square(arr))
        kon = true;
    else
    {
        fp(i, 0, 4)
        {
            fp(j, 0, 4)
            {
                if (arr[i][j] != '#')
                {
                    arr[i][j] = '#';
                    if (cek_square(arr))
                    {
                        kon = true;
                        break;
                    }
                    else
                    {
                        arr[i][j] = '.';
                    }
                }
                if (arr[i][j] != '.')
                {
                    arr[i][j] = '.';
                    if (cek_square(arr))
                    {
                        kon = true;
                        break;
                    }
                    else
                    {
                        arr[i][j] = '#';
                    }
                }
            }
            if (kon)
                break;
        }
    }
    if (kon)
        return "YES";
    else
        return "NO";
}

int main()
{
    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
