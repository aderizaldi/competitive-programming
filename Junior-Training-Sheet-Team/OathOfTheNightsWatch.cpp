#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil = 0;
    ll n;
    cin >> n;
    vl arr;
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr.pb(input);
    }
    sort(all(arr));
    fp(i, 1, n - 1)
    {
        if (arr[i] != arr[0] && arr[i] != arr[arr.size() - 1])
        {
            hasil++;
        }
    }

    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
