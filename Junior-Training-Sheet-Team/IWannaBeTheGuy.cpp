#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    ll n, p, q;
    cin >> n;
    bool kon = true;
    ll arr[n] = {0};
    cin >> p;
    fp(i, 0, p)
    {
        ll input;
        cin >> input;
        arr[input - 1] = 1;
    }
    cin >> q;
    fp(i, 0, q)
    {
        ll input;
        cin >> input;
        arr[input - 1] = 1;
    }
    fp(i, 0, n)
    {
        if (arr[i] != 1)
        {
            kon = false;
            break;
        }
    }
    if (kon)
        return "I become the guy.";
    else
        return "Oh, my keyboard!";
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
