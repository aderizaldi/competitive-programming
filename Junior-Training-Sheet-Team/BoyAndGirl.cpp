#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string input;
    cin >> input;
    char vocal[] = {'a', 'o', 'u', 'e', 'i'};
    set<char> str;
    fp(i, 0, input.size())
    {
        str.insert(input[i]);
    }
    if (str.size() % 2 == 0)
        return "CHAT WITH HER!";
    else
        return "IGNORE HIM!";
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
