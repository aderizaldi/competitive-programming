#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll n, a = 0, b = 0, c = 0;
    vl arr;
    cin >> n;
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr.pb(input);
        if (input == 1)
            a++;
        else if (input == 2)
            b++;
        else
            c++;
    }
    ll min = a;
    if (b < min)
        min = b;
    if (c < min)
        min = c;
    cout << min << endl;
    if (min > 0)
    {
        fp(i, 0, min)
        {
            bool nowa = true;
            bool nowb = true;
            bool nowc = true;
            fp(j, 0, arr.size())
            {
                if (!nowa && !nowb && !nowc)
                    break;
                if (arr[j] == 1 && nowa)
                {
                    cout << j + 1 << " ";
                    arr[j] = 0;
                    nowa = false;
                }
                if (arr[j] == 2 && nowb)
                {
                    cout << j + 1 << " ";
                    arr[j] = 0;
                    nowb = false;
                }
                if (arr[j] == 3 && nowc)
                {
                    cout << j + 1 << " ";
                    arr[j] = 0;
                    nowc = false;
                }
            }
            cout << endl;
        }
    }
}

int main()
{
    code();
}

/*		Ade Rizaldi		*/
