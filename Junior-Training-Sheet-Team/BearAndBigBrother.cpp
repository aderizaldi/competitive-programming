#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

int main()
{
    ll n, m;
    ll hasil = 0;
    cin >> n >> m;
    while (m >= n)
    {
        m *= 2;
        n *= 3;
        hasil++;
    }
    cout << hasil << endl;

    return 0;
}

/*		Ade Rizaldi		*/
