#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll n, t, k, d;
    cin >> n >> t >> k >> d;
    ll cake = 0;
    ll time = 0;
    ll oven = 1;
    ll byoven = 0;
    ll timeoven = 0;
    while (cake + byoven < n && byoven == 0)
    {
        time++;
        if (time % t == 0)
        {
            cake += k;
        }
        if (cake + byoven >= n)
            break;
        if (oven > 1)
        {
            timeoven++;
            if (timeoven % t == 0)
                byoven += k;
        }
        if (time % d == 0)
        {
            oven++;
        }
    }
    if (byoven == 0)
        cout << "NO" << endl;
    else
        cout << "YES" << endl;
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
