#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    bool kon = false;
    ll n, m;
    cin >> n >> m;
    fp(i, 0, n)
    {
        fp(j, 0, m)
        {
            char input;
            cin >> input;
            if (input == 'C' || input == 'M' || input == 'Y')
                kon = true;
        }
    }

    if (kon)
        return "#Color";
    else
        return "#Black&White";
}

int main()
{

    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
