#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string input;
    cin >> input;
    string hasil = input;
    ll upper = 0, lower = 0;
    fp(i, 0, input.size())
    {
        if (input[i] == tolower(input[i]))
            lower++;
        if (input[i] == toupper(input[i]))
            upper++;
    }
    if (upper > lower)
        transform(hasil.begin(), hasil.end(), hasil.begin(), ::toupper);
    else
        transform(hasil.begin(), hasil.end(), hasil.begin(), ::tolower);
    return hasil;
}

int main()
{
    cout << code() << endl;
}

/*		Ade Rizaldi		*/
