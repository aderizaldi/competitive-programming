#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll r1, r2, c1, c2, d1, d2, a, b, c, d;
    cin >> r1 >> r2;
    cin >> c1 >> c2;
    cin >> d1 >> d2;

    bool kon = false;
    fp(i, 1, 10)
    {
        fp(j, 1, 10)
        {
            if (i + j == r1)
            {
                fp(k, 1, 10)
                {
                    if (k + i == c1 && k + j == d2)
                    {
                        fp(l, 1, 10)
                        {
                            if (l + j == c2 && l + k == r2 && l + i == d1)
                            {
                                set<ll> myset;
                                myset.insert(i);
                                myset.insert(j);
                                myset.insert(k);
                                myset.insert(l);
                                if (myset.size() != 4)
                                    continue;
                                kon = true;
                                a = i;
                                b = j;
                                c = k;
                                d = l;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    if (kon)
        cout << a << " " << b << endl
             << c << " " << d << endl;
    else
        cout << -1 << endl;
}

int main()
{
    code();
    return 0;
}

/*		Ade Rizaldi		*/
