#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll before, hasil = 1, n;
    cin >> n;
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        if (i == 0)
        {
            before = input;
            continue;
        }
        if (before != input)
        {
            hasil++;
            before = input;
        }
    }

    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
