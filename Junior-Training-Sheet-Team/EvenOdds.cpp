#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll k, n;
    cin >> k >> n;
    ll mid = (k / 2);
    if (k % 2 != 0)
        mid++;
    if (n <= mid)
    {
        return (n * 2) - 1;
    }
    else
    {
        n = n - mid;
        return (n * 2);
    }
}

int main()
{
    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
