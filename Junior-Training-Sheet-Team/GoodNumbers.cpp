#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

bool cek_good_number(string number, ll k)
{
    set<ll> myset;
    ll arr[k + 1];
    bool kon = false;
    for (ll i = 0; i < number.size(); i++)
    {
        ll a = number[i] - int(48);
        if (a <= k)
            myset.insert(a);
        if (myset.size() > k)
            break;
    }
    if (myset.size() == k + 1)
        kon = true;
    return kon;
}

ll code()
{
    ll hasil = 0, n, k;
    cin >> n >> k;
    vs arr;
    fp(i, 0, n)
    {
        string input;
        cin >> input;
        arr.pb(input);
    }
    fp(i, 0, arr.size())
    {
        if (cek_good_number(arr[i], k))
        {
            hasil++;
        }
    }

    return hasil;
}

int main()
{

    cout << code() << endl;

    //  Insert the code that will be timed

    return 0;
}

/*		Ade Rizaldi		*/
