#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll n;
    cin >> n;
    ll arr[n];
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr[input - 1] = i + 1;
    }
    fp(i, 0, n)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
