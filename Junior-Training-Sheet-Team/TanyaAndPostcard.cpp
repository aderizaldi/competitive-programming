#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    string s, t;
    cin >> s >> t;
    ll a = 0, b = 0;
    int arr[130] = {0};
    int arr2[130] = {0};
    fp(i, 0, t.size())
    {
        char y = t[i];
        arr[(int)y]++;
    }
    fp(i, 0, s.size())
    {
        char y = s[i];
        arr2[(int)y]++;
    }
    for (ll i = 65, j = 97; i < 91, j < 123; i++, j++)
    {
        if (arr[i] != 0 && arr2[i] != 0)
        {
            if (arr[i] >= arr2[i])
            {
                arr[i] -= arr2[i];
                a += arr2[i];
                arr2[i] = 0;
            }
            else
            {
                arr2[i] -= arr[i];
                a += arr[i];
                arr[i] = 0;
            }
        }
        if (arr[j] != 0 && arr2[j] != 0)
        {
            if (arr[j] >= arr2[j])
            {
                arr[j] -= arr2[j];
                a += arr2[j];
                arr2[j] = 0;
            }
            else
            {
                arr2[j] -= arr[j];
                a += arr[j];
                arr[j] = 0;
            }
        }
    }
    for (ll i = 65, j = 97; i < 91, j < 123; i++, j++)
    {
        if (arr[i] != 0 && arr2[j] != 0)
        {
            if (arr[i] >= arr2[j])
            {
                arr[i] -= arr2[j];
                b += arr2[j];
                arr2[j] = 0;
            }
            else
            {
                arr2[j] -= arr[i];
                b += arr[i];
                arr[i] = 0;
            }
        }
        if (arr[j] != 0 && arr2[i] != 0)
        {
            if (arr[j] >= arr2[i])
            {
                arr[j] -= arr2[i];
                b += arr2[i];
                arr2[i] = 0;
            }
            else
            {
                arr2[i] -= arr[j];
                b += arr[j];
                arr[j] = 0;
            }
        }
    }
    // cout << endl;
    // for (ll i = 65, j = 97; i < 91, j < 123; i++, j++)
    // {
    //     cout << arr2[i] << " " << arr2[j] << "         " << arr[i] << " " << arr[j] << endl;
    // }
    // cout << endl;

    // fp(i, 0, s.size())
    // {
    //     char y = s[i];
    //     if (arr[(int)y] > 0)
    //     {
    //         a++;
    //         arr[(int)y]--;
    //     }
    //     else if ((int)y > 96 && arr[(int)y - 32] > 0)
    //     {
    //         b++;
    //         arr[(int)y - 32]--;
    //     }
    //     else if ((int)y < 91 && arr[(int)y + 32] > 0)
    //     {
    //         b++;
    //         arr[(int)y + 32]--;
    //     }
    // }

    // fp(i, 0, s.size())
    // {
    //     bool cond = false;
    //     fp(j, 0, t.size())
    //     {
    //         if (s[i] == t[j])
    //         {
    //             cond = true;
    //             t[j] = '-';
    //             break;
    //         }
    //     }
    //     if (cond)
    //         a++;
    //     else
    //         b++;
    // }
    // cout << endl
    //      << (int)'a' << " " << (int)'A' << endl;
    cout
        << a << " " << b << endl;
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
