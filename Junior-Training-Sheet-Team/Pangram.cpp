#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    ll n;
    string str;
    cin >> n;
    cin >> str;
    set<char> myset;
    fp(i, 0, n)
    {
        char z = tolower(str[i]);
        myset.insert(z);
    }
    if (myset.size() == 26)
        return "YES";
    else
        return "NO";
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
