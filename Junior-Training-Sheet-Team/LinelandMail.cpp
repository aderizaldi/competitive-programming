#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll longest(ll arr[], ll index, ll n)
{
    if ((arr[n - 1] - arr[index]) > (arr[index] - arr[0]))
        return arr[n - 1] - arr[index];
    else
        return arr[index] - arr[0];
}

ll shortest(ll arr[], ll index)
{
    if ((arr[index] - arr[index - 1]) < (arr[index + 1] - arr[index]))
        return arr[index] - arr[index - 1];
    else
        return arr[index + 1] - arr[index];
}

int main()
{
    ll n;
    cin >> n;
    ll arr[n];
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr[i] = input;
    }

    fp(i, 0, n)
    {
        if (i != 0 && i != n - 1)
            cout << shortest(arr, i) << " " << longest(arr, i, n) << endl;
        else if (i == 0)
            cout << arr[i + 1] - arr[i] << " " << arr[n - 1] - arr[i] << endl;
        else if (i == n - 1)
            cout << arr[i] - arr[i - 1] << " " << arr[i] - arr[0] << endl;
    }
    return 0;
}

/*		Ade Rizaldi		*/
