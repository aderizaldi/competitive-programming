#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll n, ic, sad = 0;
    cin >> n >> ic;
    fp(i, 0, n)
    {
        char op;
        ll ice;
        cin >> op >> ice;
        if (op == '+')
        {
            ic += ice;
        }
        if (op == '-')
        {
            if (ic < ice)
            {
                sad++;
            }
            else
            {
                ic -= ice;
            }
        }
    }
    cout << ic << " " << sad;
}

int main()
{
    code();
    return 0;
}

/*		Ade Rizaldi		*/
