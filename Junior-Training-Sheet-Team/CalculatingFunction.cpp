#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll n;
    cin >> n;
    ll mid = n / 2;
    if (n % 2 != 0)
        mid += 1;
    ll hasil = -1 * mid;
    if (n % 2 == 0)
        hasil += n;
    return hasil;
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
