#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll a, b;
    cin >> a >> b;
    ll c = a - b;
    string cc = to_string(c);
    ll index = cc.size();
    if (b == 0)
        return a;
    fm(i, 0, cc.size())
    {
        string ccc = cc;
        ccc[i] = '9';
        ll sem = stoll(ccc);
        index = i;
        if (a - sem > b || a - sem < 0)
            break;
        cc = ccc;
    }
    if (cc[index] != '9')
    {
        while (true)
        {
            string ccc = cc;
            string number = "";
            number += ccc[index];
            ll num = stoll(number);
            num++;
            number = to_string(num);
            ccc[index] = number[0];
            ll sem = stoll(ccc);
            if (a - sem > b || a - sem < 0)
                break;
            cc = ccc;
        }
    }
    while (true)
    {
        string ccc = cc;
        ccc += "9";
        if (ccc.size() > 18)
            break;
        ll sem = stoll(ccc);
        if (a - sem > b || a - sem < 0)
            break;
        cc = ccc;
    }

    int init = 1;
    string myhasil = cc;
    while (true)
    {
        string ccc = cc;
        string myinit = to_string(init);
        myinit += ccc;
        ll sem = stoll(myinit);
        if (a - sem > b || a - sem < 0)
            break;
        myhasil = myinit;
        init++;
    }
    ll hasil = stoll(myhasil);
    return hasil;
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
