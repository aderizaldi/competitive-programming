#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string str;
    cin >> str;
    string hasil = "";
    fp(i, 0, str.size())
    {
        ll number = str[i] - int(48);
        ll now = 9 - number;
        string z;
        if (i == 0 && number == 9)
            z = to_string(number);
        else if (now < number)
            z = to_string(now);
        else
            z = to_string(number);
        hasil += z;
    }
    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
