#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll prime[100001] = {0};

void my_prime()
{
    fp(i, 0, 100001)
    {
        if (i == 0 || i == 1)
        {
            prime[i] = 1;
        }
        for (int j = 2; j <= sqrt(i); ++i)
        {
            if (i % j == 0)
            {
                prime[i] = 1;
                break;
            }
        }
    }
}

ll code()
{
    my_prime();
    ll hasil = 0;
    ll n, m;
    cin >> n >> m;
    ll arr[n][m];
    bool kon = false;

    ll minrow = 100000;
    ll iminrow = -1;
    fp(i, 0, n)
    {
        ll counter = 0;
        fp(j, 0, m)
        {
            ll input;
            cin >> input;
            if (prime[input] != 0)
                counter++;
            arr[i][j] = input;
        }
        if (counter == 0)
            kon = true;
        else if (counter < minrow && !kon)
        {
            minrow = counter;
            iminrow = i;
        }
    }

    // fp(i, 0, n)
    // {
    //     fp(j, 0, m)
    //     {
    //         cout << arr[i][j] << " ";
    //     }
    //     cout << endl;
    // }

    ll mincol = 100000;
    ll imincol = -1;
    if (kon)
        return 0;
    else
    {
        fp(i, 0, m)
        {
            ll counter = 0;
            fp(j, 0, n)
            {
                if (prime[arr[j][i]] != 0)
                    counter++;
            }
            if (counter == 0)
                return 0;
            else if (counter < mincol)
            {
                mincol = counter;
                imincol = i;
            }
        }

        if (minrow < mincol)
        {
            fp(i, 0, m)
            {
                while (prime[arr[iminrow][i]] != 0)
                {
                    arr[iminrow][i]++;
                    hasil++;
                }
            }
        }
        else
        {
            fp(i, 0, n)
            {
                while (prime[arr[i][imincol]] != 0)
                {
                    arr[i][imincol]++;
                    hasil++;
                }
            }
        }
        return hasil;
    }
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
