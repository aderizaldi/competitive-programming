#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    vl arr;
    ll n;
    cin >> n;
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr.pb(input);
    }
    ll m;
    cin >> m;
    fp(i, 0, m)
    {
        ll a, b;
        cin >> a >> b;
        a--, b--;
        ll sem = arr[a];
        arr[a] = 0;
        if (a == 0)
        {
            arr[a + 1] += sem - (b + 1);
        }
        else if (a == arr.size() - 1)
        {
            arr[a - 1] += b;
        }
        else
        {
            arr[a + 1] += sem - (b + 1);
            arr[a - 1] += b;
        }
    }
    fp(i, 0, n)
    {
        cout << arr[i] << endl;
    }
}

int main()
{
    code();
    return 0;
}

/*		Ade Rizaldi		*/
