#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    char LR;
    string hasil = "";
    cin >> LR;
    string str;
    cin >> str;
    string keyboard[] = {"qwertyuiop",
                         "asdfghjkl;",
                         "zxcvbnm,./"};
    fp(i, 0, str.size())
    {
        fp(j, 0, 3)
        {
            bool status = false;
            fp(k, 0, keyboard[j].size())
            {
                if (str[i] == keyboard[j][k])
                {
                    if (LR == 'R')
                    {
                        hasil += keyboard[j][k - 1];
                    }
                    if (LR == 'L')
                    {
                        hasil += keyboard[j][k + 1];
                    }
                    status = true;
                    break;
                }
            }
            if (status)
                break;
        }
    }
    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
