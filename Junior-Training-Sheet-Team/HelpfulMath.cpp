#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    string input;
    vl arr;
    cin >> input;
    fp(i, 0, input.size())
    {
        if (input[i] == '1' || input[i] == '2' || input[i] == '3')
        {
            ll j = input[i] - 48;
            arr.pb(j);
        }
    }
    sort(all(arr));
    fp(i, 0, arr.size())
    {
        if (i == 0)
            cout << arr[i];
        else
            cout << '+' << arr[i];
    }
    cout << endl;
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
