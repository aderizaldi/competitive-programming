#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll factorial(ll x, ll y)
{
    ll hasil = 1;
    fp(i, x, y + 1)
    {
        hasil *= i;
    }
    return hasil;
}

ll code()
{
    ll hasil = 0;
    ll n, m, t;
    cin >> n >> m >> t;
    if (n + m == t)
        return 1;
    fp(i, 4, t)
    {
        ll aa = 1, bb = 1;
        if (i > n || t - i > m)
            continue;
        if (i > n - i)
        {
            ll counter = 1;
            while (counter <= (n - i))
            {
                if (aa % counter == 0)
                {
                    aa = aa / counter;
                    counter++;
                }
                else
                    break;
            }
            fp(j, i + 1, n + 1)
            {
                // if (aa % factorial(1, n - i) == 0)
                //     aa = aa / factorial(1, n - i);
                aa *= j;
                while (counter <= (n - i))
                {
                    if (aa % counter == 0)
                    {
                        aa = aa / counter;
                        counter++;
                    }
                    else
                        break;
                }
            }
            // aa = factorial(i + 1, n);
            // aa = aa / factorial(1, n - i);
        }
        else
        {
            ll counter = 1;
            while (counter <= i)
            {
                if (aa % counter == 0)
                {
                    aa = aa / counter;
                    counter++;
                }
                else
                    break;
            }
            fp(j, (n - i) + 1, n + 1)
            {
                // if (aa % factorial(1, i) == 0)
                //     aa = aa / factorial(1, i);
                aa *= j;
                while (counter <= i)
                {
                    if (aa % counter == 0)
                    {
                        aa = aa / counter;
                        counter++;
                    }
                    else
                        break;
                }
            }
            // aa = factorial((n - i) + 1, n);
            // aa = aa / factorial(1, i);
        }
        if ((t - i) > (m - (t - i)))
        {
            ll counter = 1;
            while (counter <= (m - (t - i)))
            {
                if (bb % counter == 0)
                {
                    bb = bb / counter;
                    counter++;
                }
                else
                    break;
            }
            fp(j, (t - i) + 1, m + 1)
            {
                // if (bb % factorial(1, (m - (t - i))) == 0)
                //     bb = bb / factorial(1, (m - (t - i)));
                bb *= j;
                while (counter <= (m - (t - i)))
                {
                    if (bb % counter == 0)
                    {
                        bb = bb / counter;
                        counter++;
                    }
                    else
                        break;
                }
            }
            // bb = factorial((t - i) + 1, m);
            // bb = bb / factorial(1, (m - (t - i)));
        }
        else
        {
            ll counter = 1;
            while (counter <= (t - i))
            {
                if (bb % counter == 0)
                {
                    bb = bb / counter;
                    counter++;
                }
                else
                    break;
            }
            fp(j, m - (t - i) + 1, m + 1)
            {
                // if (bb % factorial(1, (t - i)) == 0)
                //     bb = bb / factorial(1, (t - i));
                bb *= j;
                while (counter <= (t - i))
                {
                    if (bb % counter == 0)
                    {
                        bb = bb / counter;
                        counter++;
                    }
                    else
                        break;
                }
            }
            // bb = factorial((m - (t - i)) + 1, m);
            // bb = bb / factorial(1, (t - i));
        }
        // ll aa = factorial(n) / (factorial(i) * factorial(n - i));
        // ll bb = factorial(m) / (factorial(t - i) * factorial(m - (t - i)));
        // cout << i << " " << t - i << " = " << aa << " " << bb << endl;
        hasil += (aa * bb);
    }
    // cout << endl;

    return hasil;
}

int main()
{
    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
