#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string input;
    string hasil;
    cin >> input;
    if (input.size() <= 10)
        hasil = input;
    else
        hasil = input[0] + to_string(input.size() - 2) + input[input.size() - 1];
    return hasil;
}

int main()
{
    ll n;
    cin >> n;
    fp(i, 1, n + 1)
    {
        cout << code() << endl;
    }

    return 0;
}

/*		Ade Rizaldi		*/
