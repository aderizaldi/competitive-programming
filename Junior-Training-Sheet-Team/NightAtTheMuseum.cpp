#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil = 0, start = 0;
    string alpha = "abcdefghijklmnopqrstuvwxyz", input;
    cin >> input;
    fp(i, 0, input.size())
    {
        fp(j, 0, alpha.size())
        {
            if (input[i] == alpha[j])
            {
                if (start > j)
                {
                    ll a = start - j;
                    ll b = 26 - start + j;
                    if (a > b)
                        hasil += b;
                    else
                        hasil += a;
                }
                else if (start < j)
                {
                    ll a = j - start;
                    ll b = 26 - j + start;
                    if (a > b)
                        hasil += b;
                    else
                        hasil += a;
                }
                start = j;
                break;
            }
        }
    }

    return hasil;
}

int main()
{

    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
