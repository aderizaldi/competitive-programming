#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    ll a = 0, b = 0;
    vl arr;
    ll n;
    cin >> n;
    fp(i, 0, n)
    {
        ll input;
        cin >> input;
        arr.pb(input);
    }
    ll aa = 0, bb = arr.size() - 1;
    fp(i, 0, n)
    {
        ll hasil = 0;
        if (arr[aa] > arr[bb])
        {
            hasil = arr[aa];
            arr[aa] = -1;
            aa++;
        }
        else
        {
            hasil = arr[bb];
            arr[bb] = -1;
            bb--;
        }
        if (i % 2 == 0)
            a += hasil;
        else
            b += hasil;
    }
    cout << a << " " << b << endl;
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
