#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    bool kon = true;
    vs arr;
    ll n;
    cin >> n;
    ll m = n - 1;
    fp(i, 0, n)
    {
        string input;
        cin >> input;
        arr.pb(input);
    }
    string s = arr[0];
    char c = s[0], notc = s[1];
    if (c == notc)
        kon = false;
    else
    {
        fp(i, 0, n)
        {
            string str = arr[i];
            fp(j, 0, str.size())
            {
                if (j == i || abs(j - m) == i)
                {
                    if (str[j] != c)
                    {
                        kon = false;
                        break;
                    }
                }
                else if (str[j] != notc)
                {
                    kon = false;
                    break;
                }
            }
        }
    }

    if (kon)
        return "YES";
    else
        return "NO";
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
