#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

bool cek_prime(ll n)
{
    bool prime = true;
    fp(i, 2, sqrt(n) + 1)
    {
        if (n % i == 0)
        {
            prime = false;
            break;
        }
    }
    return prime;
}

string code()
{
    ll n, m;
    cin >> n >> m;
    bool kon = true;
    if (!cek_prime(m))
        return "NO";
    else
    {
        fp(i, n + 1, m)
        {
            if (cek_prime(i))
            {
                kon = false;
                break;
            }
        }
        if (kon)
            return "YES";
        else
            return "NO";
    }
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
