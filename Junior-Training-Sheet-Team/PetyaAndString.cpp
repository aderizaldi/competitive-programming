#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil;
    string a, b;
    cin >> a;
    cin >> b;
    transform(a.begin(), a.end(), a.begin(), ::toupper);
    transform(b.begin(), b.end(), b.begin(), ::toupper);
    if (a == b)
        hasil = 0;
    else if (a > b)
        hasil = 1;
    else
        hasil = -1;
    return hasil;
}

int main()
{
    cout << code() << endl;
}

/*		Ade Rizaldi		*/
