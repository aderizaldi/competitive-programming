#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil, x, y;
    fp(i, 0, 5)
    {
        fp(j, 0, 5)
        {
            ll input;
            cin >> input;
            if (input == 1)
            {
                x = j;
                y = i;
            }
        }
    }
    hasil = abs(x - 2) + abs(y - 2);
    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
