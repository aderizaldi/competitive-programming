#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil = 1;
    string s, t;
    cin >> s;
    cin >> t;
    ll ss = 0, tt = 0;
    while (ss != s.size() && tt != t.size())
    {
        if (s[ss] == t[tt])
        {
            ss++;
            tt++;
            hasil++;
        }
        else
        {
            tt++;
        }
    }
    return hasil;
}

int main()
{
    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
