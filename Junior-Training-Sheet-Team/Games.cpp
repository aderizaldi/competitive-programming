#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

struct Jersey
{
    ll home;
    ll away;
};

ll code()
{
    ll hasil = 0;
    ll n;
    cin >> n;
    Jersey jersey[n];
    fp(i, 0, n)
    {
        ll home, away;
        cin >> home >> away;
        jersey[i].home = home;
        jersey[i].away = away;
    }

    fp(i, 0, n - 1)
    {
        fp(j, i + 1, n)
        {
            if (jersey[i].home == jersey[j].away)
            {
                hasil++;
            }
            if (jersey[i].away == jersey[j].home)
            {
                hasil++;
            }
        }
    }

    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
