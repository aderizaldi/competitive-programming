#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    vector<char> left, right;
    string str, str2;
    cin >> str;
    cin >> str2;
    bool kon = false;
    fp(i, 0, str.size())
    {
        if (str[i] == '|')
            kon = true;
        else if (!kon)
            left.pb(str[i]);
        else
            right.pb(str[i]);
    }
    fp(i, 0, str2.size())
    {
        if (left.size() > right.size())
            right.pb(str2[i]);
        else
            left.pb(str2[i]);
    }
    if (left.size() != right.size())
    {
        cout << "Impossible" << endl;
    }
    else
    {
        fp(i, 0, left.size())
        {
            cout << left[i];
        }
        cout << "|";
        fp(i, 0, right.size())
        {
            cout << right[i];
        }
    }
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
