#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil = 0;
    ll n;
    cin >> n;
    fp(i, 0, n)
    {
        string input;
        cin >> input;
        if (input == "Tetrahedron")
            hasil += 4;
        else if (input == "Cube")
            hasil += 6;
        else if (input == "Octahedron")
            hasil += 8;
        else if (input == "Dodecahedron")
            hasil += 12;
        else if (input == "Icosahedron")
            hasil += 20;
    }

    return hasil;
}

int main()
{

    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
