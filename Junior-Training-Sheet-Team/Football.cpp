#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    ll n;
    cin >> n;
    string a = "", b = "";
    ll aa = 0, bb = 0;
    fp(i, 0, n)
    {
        string input;
        cin >> input;
        if (i == 0)
        {
            a = input;
            aa++;
        }
        else
        {
            if (input == a)
                aa++;
            else
            {
                b = input;
                bb++;
            }
        }
    }
    if (aa > bb)
        return a;
    else
        return b;
}

int main()
{

    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
