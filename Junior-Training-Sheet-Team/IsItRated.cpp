#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

struct Rating
{
    ll a;
    ll b;
};

string code()
{
    ll n;
    cin >> n;
    Rating arr[n];
    bool kon = false;
    fp(i, 0, n)
    {
        ll a, b;
        cin >> a >> b;
        arr[i].a = a;
        arr[i].b = b;
        if (a != b)
            kon = true;
    }

    if (kon)
        return "rated";
    else
    {
        bool kon2 = false;
        fp(i, 1, n)
        {
            if (arr[i].a > arr[i - 1].a)
            {
                kon2 = true;
                break;
            }
        }
        if (kon2)
            return "unrated";
        else
            return "maybe";
    }
}

int main()
{
    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
