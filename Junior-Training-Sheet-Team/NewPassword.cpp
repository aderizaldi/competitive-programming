#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string alpha = "abcdefghijklmnopqrstuvwxyz";
    ll n, k;
    cin >> n >> k;
    string newpass = alpha.substr(0, k);
    string distinc = newpass.substr(0, n - k);
    string distinc2 = distinc;
    if (n / k >= 2)
    {
        fp(i, 0, n / k + 1)
        {
            distinc2 += distinc;
        }
    }
    newpass += distinc2;
    string hasil = newpass.substr(0, n);

    return hasil;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
