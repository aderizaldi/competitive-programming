#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

string code()
{
    string anton = "Anton", danik = "Danik", tie = "Friendship";
    ll n, a = 0, d = 0;
    cin >> n;
    string str;
    cin >> str;
    fp(i, 0, n)
    {
        if (str[i] == 'D')
        {
            d++;
        }
        else if (str[i] == 'A')
        {
            a++;
        }
    }
    if (a > d)
        return anton;
    else if (a < d)
        return danik;
    else
        return tie;
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
