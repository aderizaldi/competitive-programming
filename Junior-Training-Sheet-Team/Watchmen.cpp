#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

struct Point
{
    ll x;
    ll y;
};

ll code()
{
    ll hasil = 0;
    ll n;
    cin >> n;
    Point arr[n];
    fp(i, 0, n)
    {
        ll a, b;
        cin >> a >> b;
        arr[i].x = a;
        arr[i].y = b;
    }

    fp(i, 0, n - 1)
    {
        ll x1 = arr[i].x;
        ll y1 = arr[i].y;
        fp(j, i + 1, n)
        {
            ll x2 = arr[j].x;
            ll y2 = arr[j].y;
            ll hasil2 = abs(x1 - x2) + abs(y1 - y2);
            ll hasil1 = ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
            if (hasil1 == hasil2 * hasil2)
                hasil++;
        }
    }
    return hasil;
}

int main()
{
    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
