#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define vs vector<string>
#define pb push_back
#define fp(i, a, n) for (ll i = a; i < n; i++)
#define fm(i, a, n) for (ll i = n - 1; i >= a; i--)
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil, n;
    cin >> n;
    hasil = n % 4;
    if (n == 0)
        return 1;
    else
    {
        if (hasil == 0)
            return 6;
        else if (hasil == 1)
            return 8;
        else if (hasil == 2)
            return 4;
        else
            return 2;
    }
}

int main()
{

    cout << code() << endl;

    return 0;
}

/*		Ade Rizaldi		*/
