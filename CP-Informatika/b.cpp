#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define pb push_back
#define fp(a, n) for (ll i = a; i < n; i++)
#define fm(a, n) for (ll i = n - 1; i >= a; i--)
#define print(x)        \
    fp(0, x.size())     \
            cout        \
        << x[i] << ' '; \
    cout << endl;
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

ll code()
{
    ll hasil = 0, n;
    vl arr;
    cin >> n;
    fp(0, n)
    {
        ll input;
        cin >> input;
        arr.pb(input);
    }
    sort(all(arr));
    if (arr.size() > 1)
    {
        for (int i = arr.size() - 1; i >= 0; i -= 2)
        {
            hasil += arr[i];
        }
    }
    else
    {
        hasil += arr[0];
    }

    return hasil;
}

int main()
{

    cout << code() << endl;
    return 0;
}

/*		Ade Rizaldi		*/
