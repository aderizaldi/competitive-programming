#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define pb push_back
#define fp(a, n) for (ll i = a; i < n; i++)
#define fm(a, n) for (ll i = n - 1; i >= a; i--)
#define print(x)        \
    fp(0, x.size())     \
            cout        \
        << x[i] << ' '; \
    cout << endl;
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

int main()
{
    ll x, y;
    cin >> x >> y;
    ll xx = x, yy = y;
    ll hasil = 0;
    if (x == y)
        hasil = x;
    else
    {
        while (true)
        {
            if (xx > y)
            {
                if (xx % y == 0 && xx != y)
                    break;
            }
            else
            {
                if (y % xx == 0 && y != xx)
                    break;
            }
            if (yy > x)
            {
                if (yy % x == 0 && yy != x)
                    break;
            }
            else
            {
                if (x % yy == 0 && x != yy)
                    break;
            }
            xx++;
            yy++;
            hasil++;
        }
    }
    cout << hasil << endl;
    return 0;
}

/*		Ade Rizaldi		*/
