#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define pb push_back
#define fp(a, n) for (ll i = a; i < n; i++)
#define fm(a, n) for (ll i = n - 1; i >= a; i--)
#define print(x)        \
    fp(0, x.size())     \
            cout        \
        << x[i] << ' '; \
    cout << endl;
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

void code()
{
    string s;
    ll f, n;
    cin >> n;
    string arrjudul[n], arr1[n];
    ll arrnilai[n], arr2[n];
    fp(0, n)
    {
        cin >> arrjudul[i] >> arrnilai[i];
    }
    for (int i = 0; i < n; i++)
    {
        ll bigger = arrnilai[0];
        ll index = 0;
        for (int j = 0; j < n; j++)
        {
            if (arrnilai[j] > bigger)
            {
                bigger = arrnilai[j];
                index = j;
            }
        }
        arr1[i] = arrjudul[index];
        arr2[i] = arrnilai[index];
        arrnilai[index] = -1;
    }
    fp(0, n)
    {
        cout << arr1[i] << " " << arr2[i] << endl;
    }
}

int main()
{
    code();

    return 0;
}

/*		Ade Rizaldi		*/
