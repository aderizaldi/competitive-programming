#include <bits/stdc++.h>
#define ll long long
#define vl vector<ll>
#define pb push_back
#define fp(a, n) for (ll i = a; i < n; i++)
#define fm(a, n) for (ll i = n - 1; i >= a; i--)
#define print(x)        \
    fp(0, x.size())     \
            cout        \
        << x[i] << ' '; \
    cout << endl;
#define all(x) x.begin(), x.end()
#define clr(x) memset(x, 0, sizeof(x))

using namespace std;

bool code()
{
    bool hasil = false;
    double a, b, c;
    cin >> a >> b >> c;
    if (a + b == c)
        hasil = true;
    else if (a - b == c || b - a == c)
        hasil = true;
    else if (a * b == c)
        hasil = true;
    else
    {
        if ((a == 0 && b != 0) || (b == 0 && a != 0))
        {
            if (c == 0)
                hasil = true;
        }
        else if (a != 0 && b != 0)
        {
            if (a / b == c || b / a == c)
                return true;
        }
    }

    return hasil;
}

int main()
{
    if (code())
        cout << "YA" << endl;
    else
        cout << "TIDAK" << endl;

    return 0;
}

/*		Ade Rizaldi		*/
